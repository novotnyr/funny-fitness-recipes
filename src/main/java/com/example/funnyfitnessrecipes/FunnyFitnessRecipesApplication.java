package com.example.funnyfitnessrecipes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
public class FunnyFitnessRecipesApplication {
    private List<String> recipes = Arrays.asList("Low-carb pečivo z chleba",
                "Lienka sendviče",
                "Low-fat pečený bôčik"
            );

    @GetMapping("/recipes")
    public List<String> getRecipe() {
        return recipes;
    }

    @GetMapping("/recipes/{recipeId}")
    public String getOneRecipe(@PathVariable Integer recipeId) {
        return recipes.get(recipeId);
    }


    public static void main(String[] args) {
        SpringApplication.run(FunnyFitnessRecipesApplication.class, args);
    }

}
