FROM openjdk:11-jdk-slim
COPY [ "./target/funny-fitness-recipes-0.0.1-SNAPSHOT.jar", "/app.jar" ]
ENTRYPOINT [ "java", "-jar", "/app.jar" ]
